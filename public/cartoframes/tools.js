function saveAs(uri, filename) {
    var link = document.createElement('a');
    if (typeof link.download === 'string') {
        link.href = uri;
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    } else {
        window.open(uri);
    };
};

function saveImage() {
    document.querySelectorAll(".download").forEach(function(el){el.style.display ="none";});
    document.querySelectorAll(".lab-link").forEach(function(li){li.style.textDecoration ="none";});
    try {
        document.querySelectorAll(".frameChanger").forEach(function(fc){fc.style.display ="none";});
    } catch(e) {}; 
    document.querySelector("#scrollmsg").style.display ="none";
    html2canvas(document.querySelector('#mapcanvas'),{
        scale: 2
    }).then(function(canvas) {
    saveAs(canvas.toDataURL('image/png',1), 'mapa-labcidade.png');
    });
    document.querySelectorAll(".download").forEach(function(el){el.style.display ="initial";});
    document.querySelectorAll(".lab-link").forEach(function(li){li.style.textDecoration ="underline";});
    try {
        document.querySelectorAll(".frameChanger").forEach(function(fc){fc.style.display ="initial";});
    } catch(e) {};
};

function showtxt() {
    $("#scrollmsg").show("slow","swing");
    $("#scrollmsg").delay(1500).hide("slow","swing");
}

function removemsg() {
    document.querySelector('#mapcanvas').onmouseenter=null;
    $("#scrollmsg").hide();
}

function removeBalloon(how= null) {
    $(".infoBalloon").hide(how);
}